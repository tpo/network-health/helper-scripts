#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2021, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import math

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *


print("== Output format ==")
print(
    "%s: %s [%s %%] [[%s %%]] %s"
    % (
        "count",
        "version",
        "percent weight",
        "percent advertised bandwidth",
        "(MAJOR version)"
    )
)

print("\n== Relays == ")

relays = dict(
    filter(lambda i: Flag.RUNNING in i[1].flags, rse.routers.items())
)
relays_versions = set(map(lambda desc: str(desc.version), relays.values()))
relays_major_versions = set(map(lambda v: v[:5], relays_versions))
all_relays_versions = sorted(relays_versions.union(relays_major_versions))

sds_in_relays = dict(filter(lambda i: i[0] in relays.keys(), sds.items()))

versions_count = {}
weight_sum = {}
adv_bw_sum = {}

for version in all_relays_versions:
    if len(version) == 5:
        relays_with_version = list(
            filter(
                lambda desc: str(desc.version).startswith(version),
                relays.values(),
            )
        )
        sds_with_version = list(
            filter(
                lambda desc: str(desc.tor_version).startswith(version),
                sds_in_relays.values(),
            )
        )
    else:
        relays_with_version = list(
            filter(lambda desc: str(desc.version) == version, relays.values())
        )
        sds_with_version = list(
            filter(
                lambda desc: str(desc.tor_version) == version,
                sds_in_relays.values(),
            )
        )

    versions_count[version] = len(relays_with_version)
    weight_sum[version] = sum(
        list(map(lambda desc: desc.bandwidth * 1000, relays_with_version))
    )
    adv_bw_sum[version] = sum(
        list(
            map(
                lambda desc: min(
                    desc.average_bandwidth,
                    desc.observed_bandwidth,
                    desc.burst_bandwidth,
                ),
                sds_with_version,
            )
        )
    )

relays_total_weight = sum(
    list(map(lambda desc: desc.bandwidth * 1000, relays.values()))
)
relays_total_adv_bw = sum(
    list(
        map(
            lambda desc: min(
                desc.average_bandwidth,
                desc.observed_bandwidth,
                desc.burst_bandwidth,
            ),
            sds_in_relays.values(),
        )
    )
)

percent_weight = {}
percent_adv_bw = {}
for version in all_relays_versions:
    percent_weight[version] = (weight_sum[version] / relays_total_weight) * 100
    percent_adv_bw[version] = (adv_bw_sum[version] / relays_total_adv_bw) * 100

for v in all_relays_versions:
    print(
        "%s%d: %s%s[%.2f %%] [%.2f %%] %s"
        % (
            " " * (4 - (int(math.log10(versions_count[v])) + 1)),
            versions_count[v],
            v,
            (18 - len(v.__str__())) * " ",
            percent_weight[v],
            percent_adv_bw[v],
            ("(MAJOR)" if len(v) == 5 else ""),
        )
    )

print("\n== Exits == ")

exits = dict(
    filter(
        lambda i: Flag.EXIT in i[1].flags
        and Flag.BADEXIT not in i[1].flags
        and i[1].exit_policy.can_exit_to(None, 443, False)
        and Flag.RUNNING in i[1].flags,
        relays.items(),
    )
)

exits_versions = set(map(lambda desc: str(desc.version), exits.values()))
exits_major_versions = set(map(lambda v: v[:5], exits_versions))
all_exits_versions = sorted(exits_versions.union(exits_major_versions))
sds_in_exits = dict(filter(lambda i: i[0] in exits.keys(), sds.items()))

for version in all_exits_versions:
    if len(version) == 5:
        exits_with_version = list(
            filter(
                lambda desc: str(desc.version).startswith(version),
                exits.values(),
            )
        )
        sds_with_version = list(
            filter(
                lambda desc: str(desc.tor_version).startswith(version),
                sds_in_exits.values(),
            )
        )
    else:
        exits_with_version = list(
            filter(lambda desc: str(desc.version) == version, exits.values())
        )
        sds_with_version = list(
            filter(
                lambda desc: str(desc.tor_version) == version,
                sds_in_exits.values(),
            )
        )

    versions_count[version] = len(exits_with_version)
    weight_sum[version] = sum(
        list(map(lambda desc: desc.bandwidth * 1000, exits_with_version))
    )
    adv_bw_sum[version] = sum(
        list(
            map(
                lambda desc: min(
                    desc.average_bandwidth,
                    desc.observed_bandwidth,
                    desc.burst_bandwidth,
                ),
                sds_with_version,
            )
        )
    )

exits_total_weight = sum(
    list(map(lambda desc: desc.bandwidth * 1000, exits.values()))
)
exits_total_adv_bw = sum(
    list(
        map(
            lambda desc: min(
                desc.average_bandwidth,
                desc.observed_bandwidth,
                desc.burst_bandwidth,
            ),
            sds_in_exits.values(),
        )
    )
)

percent_weight = {}
percent_adv_bw = {}
for version in all_exits_versions:
    percent_weight[version] = (weight_sum[version] / exits_total_weight) * 100
    percent_adv_bw[version] = (adv_bw_sum[version] / exits_total_adv_bw) * 100

for v in all_exits_versions:
    print(
        "%s%d: %s%s[%.2f %%] [%.2f %%] %s"
        % (
            " " * (4 - (int(math.log10(versions_count[v])) + 1)),
            versions_count[v],
            v,
            (18 - len(v.__str__())) * " ",
            percent_weight[v],
            percent_adv_bw[v],
            ("(MAJOR)" if len(v) == 5 else ""),
        )
    )

print("\n== Bridges == ")

bridge_versions = {}
bridge_weights = {}
bridge_total_w = 0.0

for desc in bds.values():
  version = str(desc.tor_version)
  major = version[:5]
  if version not in bridge_versions:
    bridge_versions[version] = 0
    bridge_weights[version] = 0
  if major not in bridge_versions:
    bridge_versions[major] = 0
    bridge_weights[major] = 0
  bridge_versions[version] += 1
  bridge_weights[version] += desc.observed_bandwidth
  bridge_versions[major] += 1
  bridge_weights[major] += desc.observed_bandwidth
  bridge_total_w += desc.observed_bandwidth

for v in sorted(bridge_versions.keys()):
  digits = int(math.log10(bridge_versions[v])) + 1
  if digits == 4:
    spaces = ""
  elif digits == 3:
    spaces = " "
  elif digits == 2:
    spaces = "  "
  else:
    spaces = "   "

  w_percent = (bridge_weights[v] / bridge_total_w or 1) * 100

  print("%s%d: %s%s[%.2f %%] %s" % \
        (spaces, bridge_versions[v], v, (18 - len(v.__str__())) * " ",
         w_percent, ("(MAJOR)" if len(v) == 5 else "")))
