#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2016, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *

exit_count = 0
guard_count = 0
mid_count = 0
invalid_count = 0
running_count = 0
badexit_count = 0
hsdir_count = 0

for fp, desc in rse.routers.items():
    if Flag.VALID not in desc.flags:
        invalid_count += 1
        continue
    mid_count += 1
    if Flag.RUNNING in desc.flags:
        running_count += 1
    if Flag.EXIT in desc.flags:
        exit_count += 1
        if Flag.BADEXIT in desc.flags:
            badexit_count += 1
    if Flag.GUARD in desc.flags:
        guard_count += 1
    if Flag.HSDIR in desc.flags:
        hsdir_count += 1

print("")
print("[+] Exit:    %d" % (exit_count))
print("[+] BadExit: %d" % (badexit_count))
print("[+] Guard:   %d" % (guard_count))
print("[+] Middle:  %d" % (mid_count))
print("[+] HSDir:   %d" % (hsdir_count))
print("[+] Invalid: %d" % (invalid_count))
print("[+] Running: %d" % (running_count))
