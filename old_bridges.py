#!/usr/bin/env python3

# Copyright (c) 2020, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys

from stem.descriptor import parse_file
from stem.version import Version

old35 = Version("0.3.5")
old42 = Version("0.4.2")
old43 = Version("0.4.3")
old44 = Version("0.4.4")
bridges = {}


def equals(v1, v2):
    return v1.major == v2.major and \
           v1.minor == v2.minor and \
           v1.micro == v2.micro


def is_old(tor_version):
    return equals(tor_version, old35) or equals(tor_version, old42) or equals(tor_version, old43) or equals(tor_version, old44)


with open(sys.argv[1], "rb") as desc_file:
    for desc in parse_file(desc_file,
                           descriptor_type="server-descriptor 1.0"):
        bridges[desc.fingerprint] = desc

print("Parsed {:,} bridges.".format(len(bridges)))

num_old = 0
num_contact = 0

for _, desc in bridges.items():
    if is_old(desc.tor_version):
        num_old += 1
        if desc.contact is not None:
            num_contact += 1
            print("{} ({}) -> {}.".format(desc.tor_version,
                                          desc.fingerprint,
                                          desc.contact.decode("utf-8")))
        else:
            print("{} ({}).".format(desc.tor_version,
                                    desc.fingerprint))

pct = num_contact / num_old * 100 if num_old > 0 else 0
print("{:,} bridges run old Tor version.".format(num_old))
print("{:,} ({:.1f}%) of these bridges have contact info.".format(num_contact,
                                                                  pct))
