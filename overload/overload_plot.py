#!/usr/bin/env python3

# Copyright (c) 2021, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import matplotlib.pyplot as plt
import pandas as pd

header_list = ["date", "overload general all", "overload general exits",
               "overload fd exhausted all", "overload fd exhausted exits",
               "overload ratelimits all", "overload ratelimits exits",
               "overload general bridges", "overload fd exhausted bridges",
               "overload ratelimits bridges", "overload general hsdirs",
               "overload fd exhausted hsdirs", "overload ratelimits hsdirs",
               "overload general guards", "overload fd exhausted guards",
               "overload ratelimits guards"]

x_axis = "date"
df = pd.read_csv("overload.csv", parse_dates=["date"], names=header_list)

# overload-general for relays first
y_axes = ["overload general all", "overload general exits",
          "overload general guards", "overload general hsdirs"]
filter = [x_axis] + y_axes

title = "Relays (all, exit, guard, hsdir flag) with general overload"
df[filter].plot(x=x_axis, y=y_axes, title=title)
plt.savefig("overload-general-relays.png")

# overload-fd-exhausted for relays
y_axes = ["overload fd exhausted all", "overload fd exhausted exits",
          "overload fd exhausted guards", "overload fd exhausted hsdirs"]
filter = [x_axis] + y_axes

title = "Relays (all, exit, guard, hsdir flag) with fd exhaustion \
overload"
df[filter].plot(x=x_axis, y=y_axes, title=title)
plt.savefig("overload-fd-exhausted-relays.png")

# overload-ratelimits for relays
y_axes = ["overload ratelimits all", "overload ratelimits exits",
          "overload ratelimits guards", "overload ratelimits hsdirs"]
filter = [x_axis] + y_axes

title = "Relays (all, exit, guard, hsdir flag) with ratelimits overload"
df[filter].plot(x=x_axis, y=y_axes, title=title)
plt.savefig("overload-ratelimits-relays.png")

# bridges overload
y_axes = ["overload general bridges", "overload fd exhausted bridges",
          "overload ratelimits bridges"]
filter = [x_axis] + y_axes

title = "Number of all bridges showing types of overload"
df[filter].plot(x=x_axis, y=y_axes, title=title)
plt.savefig("overload-bridges.png")
