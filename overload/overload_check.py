#!/usr/bin/env python3

# Copyright (c) 2021-2022, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import json
import time
import urllib.request
from datetime import date


def get_relay_bridge_data(bw):
    url = "https://onionoo.torproject.org/"
    if bw:
        url += "bandwidth"
    else:
        url += "details"
    req = urllib.request.Request(url)
    response = urllib.request.urlopen(req).read()
    data = json.loads(response.decode('utf-8'))

    return data["relays"], data["bridges"]


def is_timestamp_too_old(overload, timestamp):
    # Let's see if we have a timestamp which is too old. A positive result does
    # indicate that. We are only interested in timestamps that are not older
    # than 18 hours as we get otherwise too much noise in our monitoring data.
    # See: helper-scripts#17 for more details. We use 19 hours to take into
    # account that the overload monitoring scripts are run about 30 minutes
    # after the onionoo data got assembled and made available.
    diff = time.time() - timestamp/1000 - 3600 * 19
    if diff > 0:
        return True
    else:
        return False


overloads = ["overload_general_timestamp", "overload_fd_exhausted",
             "overload_ratelimits"]

relays_details, bridges_details = get_relay_bridge_data(False)
relays_bw, bridges_bw = get_relay_bridge_data(True)

relays = {}
bridges = {}
exits = []
hsdirs = []
guards = []
running_relays = []
running_bridges = []
overload_relays = {}
overload_bridges = {}
today = date.today().strftime("%m-%d-%Y")

# Let's assemble the running relays and keep track of flags we are interested
# in.
for relay in relays_details:
    fp = relay["fingerprint"]
    if relay["running"] is True:
        running_relays.append(fp)
    else:
        continue
    if "flags" in relay:
        if "Exit" in relay["flags"]:
            exits.append(fp)
        if "HSDir" in relay["flags"]:
            hsdirs.append(fp)
        if "Guard" in relay["flags"]:
            guards.append(fp)

# Now the running bridges
for bridge in bridges_details:
    if bridge["running"] is True:
        running_bridges.append(bridge["hashed_fingerprint"])

for overload in overloads:
    if overload == "overload_general_timestamp":
        relays = relays_details
        bridges = bridges_details
    else:
        relays = relays_bw
        bridges = bridges_bw

    for relay in relays:
        fp = relay["fingerprint"]
        if fp not in running_relays:
            continue
        if overload in relay:
            if overload == "overload_general_timestamp":
                timestamp = relay[overload]
            else:
                timestamp = relay[overload]["timestamp"]
        else:
            # No overload, check next relay
            continue
        if is_timestamp_too_old(overload, timestamp):
            print("WARN: Timestamp %s is too old for overload %s for \
                  relay %s. Skipping. " % (timestamp, overload, fp))
            continue
        if overload not in overload_relays:
            overload_relays[overload] = {}
        overload_relays[overload][fp] = {"timestamp": timestamp,
                                         "exit": True if fp in exits else
                                         False,
                                         "hsdir": True if fp in hsdirs else
                                         False,
                                         "guard": True if fp in guards else
                                         False}

    jsonStr = json.dumps(overload_relays)
    with open("overloaded_relays-" + today + ".json", "w") as jfile:
        jfile.write(jsonStr)

    for bridge in bridges:
        # Working around some Onionoo weirdness
        if overload == "overload_general_timestamp":
            fp = bridge["hashed_fingerprint"]
        else:
            fp = bridge["fingerprint"]
        if fp not in running_bridges:
            continue
        if overload in bridge:
            if overload == "overload_general_timestamp":
                timestamp = bridge[overload]
            else:
                timestamp = bridge[overload]["timestamp"]
        else:
            # No overload, check next bridge
            continue
        if is_timestamp_too_old(overload, timestamp):
            print("WARN: Timestamp %s is too old for overload %s for \
                  bridge %s. Skipping. " % (timestamp, overload, fp))
            continue
        if overload not in overload_bridges:
            overload_bridges[overload] = {}
        overload_bridges[overload][fp] = {"timestamp": timestamp}

    jsonStr = json.dumps(overload_bridges)
    with open("overloaded_bridges-" + today + ".json", "w") as jfile2:
        jfile2.write(jsonStr)
