#!/usr/bin/env python3

# Copyright (c) 2021, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import json
from datetime import date

relays = {}
overload_general_relays = 0
overload_general_exits = 0
overload_general_hsdirs = 0
overload_general_guards = 0
overload_fd_exhausted_relays = 0
overload_fd_exhausted_exits = 0
overload_fd_exhausted_hsdirs = 0
overload_fd_exhausted_guards = 0
overload_ratelimits_relays = 0
overload_ratelimits_exits = 0
overload_ratelimits_hsdirs = 0
overload_ratelimits_guards = 0

bridges = {}
overload_general_bridges = 0
overload_fd_exhausted_bridges = 0
overload_ratelimits_bridges = 0

today = date.today().strftime("%m-%d-%Y")

with open("overloaded_relays-" + today + ".json", "r") as read_relays_file:
    relays = json.load(read_relays_file)
with open("overloaded_bridges-" + today + ".json", "r") as read_bridges_file:
    bridges = json.load(read_bridges_file)

for key, value in relays.items():
    if key == "overload_general_timestamp":
        for _, v in value.items():
            overload_general_relays += 1
            if v["exit"] is True:
                overload_general_exits += 1
            if v["hsdir"] is True:
                overload_general_hsdirs += 1
            if v["guard"] is True:
                overload_general_guards += 1
    elif key == "overload_fd_exhausted":
        for _, v in value.items():
            overload_fd_exhausted_relays += 1
            if v["exit"] is True:
                overload_fd_exhausted_exits += 1
            if v["hsdir"] is True:
                overload_fd_exhausted_hsdirs += 1
            if v["guard"] is True:
                overload_fd_exhausted_guards += 1
    elif key == "overload_ratelimits":
        for _, v in value.items():
            overload_ratelimits_relays += 1
            if v["exit"] is True:
                overload_ratelimits_exits += 1
            if v["hsdir"] is True:
                overload_ratelimits_hsdirs += 1
            if v["guard"] is True:
                overload_ratelimits_guards += 1

if "overload_general_timestamp" in bridges:
    overload_general_bridges = len(bridges["overload_general_timestamp"].
                                   items())
if "overload_fd_exhausted" in bridges:
    overload_fd_exhausted_bridges = len(bridges["overload_fd_exhausted"].
                                        items())
if "overload_ratelimits" in bridges:
    overload_ratelimits_bridges = len(bridges["overload_ratelimits"].items())

with open("overload.csv", "a") as csv_file:
    csv_file.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(
                   today, overload_general_relays, overload_general_exits,
                   overload_fd_exhausted_relays, overload_fd_exhausted_exits,
                   overload_ratelimits_relays, overload_ratelimits_exits,
                   overload_general_bridges, overload_fd_exhausted_bridges,
                   overload_ratelimits_bridges, overload_general_hsdirs,
                   overload_fd_exhausted_hsdirs, overload_ratelimits_hsdirs,
                   overload_general_guards, overload_fd_exhausted_guards,
                   overload_ratelimits_guards))
