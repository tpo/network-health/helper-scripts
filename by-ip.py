#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2018, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys, os
import stem

from datetime import datetime
from stem import Flag
from stem.descriptor import DocumentHandler, parse_file
from stem.descriptor.remote import DescriptorDownloader

from util import load_args_as_ip

ips = []
total_bw = 0.0

load_args_as_ip(sys.argv, ips)

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *

print("[+] Testing %d IPs..." % (len(ips)))

found = {}
for fp, desc in rse.routers.items():
    for ip in ips:
        if ip in desc.address:
            found[fp] = desc
            describe(desc)
            sd = get_sd(desc)
            if sd is not None:
                total_bw += sd.observed_bandwidth

print("[+] %d relays in the consensus" % (len(found)))

print("\n== Fingerprints ==")
sort_found = sorted(found)
fp_str = ""
for fp in sort_found:
    fp_str += "%s," % (fp)
print(fp_str[:-1])

print("\n== Rules ==")
rules = []
for desc in found.values():
    rules.append("AuthDirReject %s" % (desc.address))
for rule in sorted(set(rules)):
    print(rule)

print("\n== Approved Routers ==")
rules = []
for desc in found.values():
    rules.append("!reject %s" % (desc.fingerprint))
for rule in sorted(set(rules)):
    print(rule)

print("\n== Total Bandwidth ==")
print("[+] %f MB/s" % (total_bw / 1000.0 / 1000.0))
