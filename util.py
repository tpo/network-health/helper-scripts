# -*- coding: utf-8 -*-

# Copyright (c) 2017, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import re, sys, os

def load_args_as_ip(args, ips):
    # Try to load the filename as arg.
    if len(args) > 1:
        filename = args[1]
        if os.path.exists(filename):
            print("[+] Using file %s..." % (filename))
            with open(filename, "r") as fd:
                for line in fd:
                    line = line.strip()
                    # Ignore commented fingerprint
                    if line.startswith("#"):
                        continue
                    if line not in ips:
                        ips.append(line)
                        print("  [+] Adding %s" % (line))
            if len(ips) == 0:
                print("[-] No IPs are usable");
                sys.exit(1)
        else:
            ip = args[1].strip()
            ips.append(ip)
            print("[+] Testing IP %s" % (ip))
    else:
        print("[-] Missing filename or IPs. Stopping.")
        sys.exit(1)

def load_args_as_fp(args, fps):
    fpre = re.compile("^[0-9A-Fa-f]{40}$")
    # Try to load the filename as arg.
    if len(args) > 1:
        filename = args[1]
        if os.path.exists(filename):
            print("[+] Using file %s..." % (filename))
            if os.path.islink(filename):
                print("[-] Filename %s no valid because is a symlink" % \
                      (filename))
                sys.exit(1)
            with open(filename, "r") as fd:
                for line in fd:
                    line = line.strip()
                    # Ignore commented fingerprint
                    if line.startswith("#"):
                        continue
                    if not fpre.match(line):
                        continue;
                    if line not in fps:
                        fps.append(line)
                        print("  [+] Adding %s" % (line))
            if len(fps) == 0:
                print("[-] No fingerprint is usable");
                sys.exit(1);
        else:
            # Filename is probably a fingerprint.
            fp = args[1].strip()
            if not fpre.match(fp):
                print("[-] Filename %s not found or not valid fingerprint" % \
                        (filename))
                sys.exit(1)
            fps.append(fp)
            print("[+] Testing fingerprint %s" % (fp))
    else:
        print("[-] Missing filename or fingerprint. Stopping.")
        sys.exit(1)
